from sqlalchemy import create_engine, select
from sqlalchemy.orm import sessionmaker

#den einai singleton vevaia alla tespa
class Sigleton(object):
    def __init__(self):
        engine = create_engine("postgresql://kiniti_user:14mTh3L4w!@83.212.99.115/kiniti_tilefwnia", echo=True)
        self.Session = sessionmaker(bind=engine)

    def databaseConnection(self):
        session = self.Session()
        return session
