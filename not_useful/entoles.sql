-- get account info for specific user_id
select bill.teleph_number, users.firstname, users.lastname, telephony_program.charge_mob, telephony_program.charge_home, telephony_program.charge_standard
from users
inner join bill
on users.user_id = bill.user_id
inner join telephony_program
on bill.program_id = telephony_program.program_id
where users.user_id = 1;

-- get all calls for specific user_id and specific date
select call.call_dur , call.called_number, call.call_date
from users inner join bill
on users.user_id = bill.user_id
inner join call
on call.bill_id = bill.bill_id
where users.user_id = 1 and extract (month from call.call_date) = 1;

--trigger stuff
CREATE TRIGGER update_bill_overall_due
  AFTER INSERT
  ON bill
  FOR EACH ROW
  EXECUTE PROCEDURE update_bill_overall_due_function_tr();


create or replace function update_bill_overall_due_function_tr()
returns trigger as $update_bill_overall_due_function$
	declare
		current_bill_due_value numeric;

		current_bill_number text ;

		current_bill_uid int ;

		current_bill_billid int ;

		previous_bill_overall_due_value numeric;

		overall_due_value numeric;

	begin

	drop table if exists temp1;
	drop table if exists temp2;

	select current_bill_due into current_bill_due_value
	from bill
	order by bill_id desc limit 1;

	select teleph_number into current_bill_number
	from bill
	order by bill_id desc limit 1;

	select user_id into current_bill_uid
	from bill
	order by bill_id desc limit 1;

	select bill_id into current_bill_billid
	from bill
	order by bill_id desc limit 1;

	--get second to last record based on teleph_number and user_id


	create temp table temp1 as(
	select bill_id, bill_overall_due
	from bill
	where teleph_number = current_bill_number and user_id = current_bill_uid
	order by bill_id desc limit 2) ;

	create temp table temp2 as(
	select bill_id, bill_overall_due
	from temp1
	order by  bill_id asc limit 1);

	select bill_overall_due into previous_bill_overall_due_value
	from temp2;

	overall_due_value := current_bill_due_value + previous_bill_overall_due_value;

	--update the last rows overall due
	update bill
	set bill_overall_due = overall_due_value
	where bill_id = current_bill_billid;

  return null;
	end;
$update_bill_overall_due_function$ language plpgsql;
