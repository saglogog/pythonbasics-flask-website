from sqlalchemy import Column, Integer, String, Date, Interval, Numeric, ForeignKey, Boolean
from sqlalchemy.orm import relationships, backref, relationship
from sqlalchemy.ext.declarative import declarative_base

#engine = create_engine("postgresql://kiniti_user:14mTh3L4w!@83.212.99.115/kiniti_tilefwnia", echo=True)

Base = declarative_base()

class User(Base):
    __tablename__ = 'users'

    user_id = Column(Integer, primary_key = True)
    username = Column(String)
    password = Column(String)
    email = Column(String)
    firstname = Column(String)
    lastname = Column(String)
    afm = Column(String)
    flag = Column(Boolean)
    role_id = Column(Integer, ForeignKey('role.role_id'))
    bills = relationship("Bill", backref = "users")


class Role(Base):
    __tablename__ = 'role'

    role_id = Column(Integer, primary_key = True)
    role_desc = Column(String)

    users = relationship("User", backref = "role")


class Bill(Base):
    __tablename__ = 'bill'

    bill_id = Column(Integer, primary_key = True)
    teleph_number = Column(String)
    bill_overall_due = Column(Numeric)
    current_bill_due = Column(Numeric)
    program_id = Column(String, ForeignKey('telephony_program.program_id'))
    user_id = Column(String, ForeignKey('users.user_id'))

    calls = relationship("Call", backref = ("bill"))


class Call(Base):
    __tablename__ = "call"

    call_id = Column(Integer, primary_key = True)
    bill_id = Column(Integer, ForeignKey('bill.bill_id'))
    call_dur = Column(Interval)
    called_number = Column(String)
    call_date = Column(Date)


class Telephony_Program(Base):
    __tablename__ = "telephony_program"

    program_id = Column(Integer, primary_key = True)
    program_desc = Column(String)
    charge_mob = Column(Numeric)
    charge_home = Column(Numeric)
    charge_standard = Column(Numeric)

    bills = relationship("Bill", backref = "telephony_program")
