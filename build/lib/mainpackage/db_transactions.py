from sqlalchemy import create_engine, select
from sqlalchemy.orm import sessionmaker
from dao import User, Bill, Call, Telephony_Program, Role
from passlib.handlers.sha2_crypt import sha256_crypt

# session.add_all([
#     Role(role_desc = 'Administrator: Does the administrators dooooo.'),
#     Role(role_desc = 'Seller: Tries to sell crap products to ungrateful shits.')
# ])

# session.add_all([
#     User(username = 'manny', password = str(sha256_crypt.encrypt("userpassword1", rounds = 20000, salt_size=16)),
#          email = 'fellos@gmail.com', role_id = '1'),
#     User(username = 'noma', password = str(sha256_crypt.encrypt("userpassword2", rounds = 20000, salt_size=16)),
#          email = 'fellos2@gmail.com', role_id = '2')])

class Telephony_Program_Transactions(object):
    def __init__(self):
        engine = create_engine("postgresql://kiniti_user:14mTh3L4w!@83.212.99.115/kiniti_tilefwnia", echo=True)
        self.Session = sessionmaker(bind=engine)

    def add_telephony_program(self, program_desc, charge_mob, charge_home, charge_standard):
        try:
            session = self.Session()
            session.add(Telephony_Program(program_desc = program_desc, charge_mob = charge_mob,
                                          charge_home = charge_home, charge_standard = charge_standard))
            session.commit()
        except Exception as e:
            raise

    def select_all_telephony_programs(self):
        try:
            session = self.Session()
            q = session.query(Telephony_Program).order_by(Telephony_Program.program_id)
            session.commit()
        except Exception as e:
            raise
        return q

tpt = Telephony_Program_Transactions()
# tpt.add_telephony_program('klassiko', 10, 2, 5)
# tpt.add_telephony_program('extra_sauce', 2, 2, 10)
for instance in tpt.select_all_telephony_programs():
    print (instance.program_desc, instance.charge_mob, instance.charge_home, instance.charge_standard)
