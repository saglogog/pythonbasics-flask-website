from sqlalchemy import create_engine, select
from sqlalchemy.orm import sessionmaker
from mainpackage.dao import User, Telephony_Program,Bill
from passlib.handlers.sha2_crypt import sha256_crypt

class Admin(object):
    "Subclass Admin of Users"
    def __init__(self, _session):
        self.session = _session

    def createClient(self, username_client, password_client, email_client, firstname_client, lastname_client, afm_client):
        try:
            #gnwrizoume oti to role_id = 1 gia pelates
            self.session.add(User(username = username_client, password = str(sha256_crypt.encrypt(password_client, rounds = 20000, salt_size=16)),
                                          email = email_client, firstname = firstname_client,
                                          lastname = lastname_client, afm = afm_client, role_id = 3, flag = True))
            self.session.commit()
        except Exception as e:
            raise

    def createSeller(self, username_seller, password_seller, email_seller, firstname_seller, lastname_seller, afm_seller):
        try:
            #dhmiourgia seller apo ton admin me id_seller = 2
            self.session.add(User(username = username_seller, password = str(sha256_crypt.encrypt(password_seller, rounds = 20000, salt_size=16)),
                                        email = email_seller, firstname = firstname_seller,
                                        lastname = lastname_seller, afm = afm_seller, role_id = 2, flag = True))
            self.session.commit()
        except Exception as e :
            raise

    def createTelephonyProgram(self, program_description, charge_mobile_phones, charge_home_phones, fixed_mobile):
        try:
            #dhmiourgia thlefwnikou programmatos apo ton admin
            self.session.add(Telephony_Program(program_desc = program_description, charge_mob = charge_mobile_phones,
                                          charge_home = charge_home_phones, charge_standard = fixed_mobile ))
            self.session.commit()
        except Exception as e :
            raise

    #disable not delete w/ flag
    # by default, flag is true when the user is active
    # maybe fix to throw Exception when user isnt a client???
    def removeClient(self, user_id_arg):
        try:
            q = self.session.query(User).filter(User.user_id == user_id_arg, User.role_id == 3)
            for entry in q:
                entry.flag = False
            self.session.commit()
        except Exception as e:
            raise

    #same dealio w/ removeClient
    def removeSeller(self, user_id_arg):
        try:
            q = self.session.query(User).filter(User.user_id == user_id_arg, User.role_id == 2)
            for entry in q:
                entry.flag = False
            self.session.commit()
        except Exception as e:
            raise
